﻿namespace prep.utility
{
  public delegate bool Condition<Item>(Item item);
}