namespace prep.utility.filtering
{
  public class NegatingMatch<Item> : IMatchAn<Item>
  {
    IMatchAn<Item> to_negate;

    public NegatingMatch(IMatchAn<Item> match)
    {
      to_negate = match;
    }

    public bool matches(Item item)
    {
      return ! to_negate.matches(item);
    }
  }
}